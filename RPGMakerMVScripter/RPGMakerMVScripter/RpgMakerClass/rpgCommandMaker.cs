﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGMakerMVScripter.RpgMakerClass
{
  class rpgCommandMaker
  {
    static List<rpgCommand> listOfCommand;

    public enum eRPGCommand
    {
      eMessage = 101,
      eText = 401
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sCode"></param>
    /// <param name="sIdent"></param>
    /// <param name="sParam"></param>
    public static void AddCommand(eRPGCommand sCode, int sIdent, List<object> sParam)
    {
      if (listOfCommand == null)
        listOfCommand = new List<rpgCommand>();

      listOfCommand.Add(new rpgCommand(sCode, sIdent, sParam));
    }

    /// <summary>
    /// Pour tout virer
    /// </summary>
    public static void ClearAll()
    {
      if (listOfCommand != null)
        listOfCommand.Clear();
    }

    /// <summary>
    /// Format la list des command dans le bon format avant la convertion Unicode pour le 
    /// press papier
    /// </summary>
    /// <returns></returns>
    public static string GetFormat()
    {
      string ret = "";
      if ((listOfCommand != null) && (listOfCommand.Count > 0))
      {
        if (listOfCommand[0].code.IndexOf("\"code\":101") < 0)
          listOfCommand.Insert(0, new rpgCommand(eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 }));

        foreach (rpgCommand rpg in listOfCommand)
        {
          ret += "{" + rpg.code + "," + rpg.indent + "," + rpg.parameters + "},";
        }
      }
      else
      {
        ret = "[{\"code\":118,\"indent\":0,\"parameters\":[\"eee\"]}]";
      }
      return "[" + ret.Substring(0, ret.Length - 1) + "]";
    }
  }
}
