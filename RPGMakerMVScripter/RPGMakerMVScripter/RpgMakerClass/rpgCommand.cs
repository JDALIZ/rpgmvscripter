﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGMakerMVScripter.RpgMakerClass
{
  class rpgCommand
  {

      public string code;
      public string indent;
      public string parameters;

      public rpgCommand(rpgCommandMaker.eRPGCommand c, int i, List<object> p)
      {
        code = "\"code\":" + ((int)c).ToString("000");
        indent = "\"indent\":" + i.ToString();
        parameters = "\"parameters\":[";
        if (p != null)
        {

          foreach (object obj in p)
          {
            if (obj.GetType().Name.ToLower().IndexOf("int") >= 0)
              parameters += obj.ToString() + ",";
            else if (obj.GetType().Name.ToLower().IndexOf("bool") >= 0)
              parameters += obj.ToString() + ",";
            else if (obj.GetType().Name.ToLower().IndexOf("string") >= 0)
            {
              string str = obj as string;
              if (str.Length > 0)
                parameters += "\"" + obj + "\",";
              else
                parameters += "\"\",";
            }
          }

          parameters = parameters.Substring(0, parameters.Length - 1) + "]";
        }
        else
        {
          parameters += "]";
        }
      }
    
  }
}
