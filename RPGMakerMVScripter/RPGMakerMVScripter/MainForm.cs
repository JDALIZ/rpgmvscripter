﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RPGMakerMVScripter.RpgMakerClass;

namespace RPGMakerMVScripter
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();
    }

    #region EVENTS

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void button1_Click(object sender, EventArgs e)
    {
      // Declares an IDataObject to hold the data returned from the clipboard.
      // Retrieves the data from the clipboard.
      IDataObject iData = Clipboard.GetDataObject();

      if (iData != null)
      {
        string[] fm = iData.GetFormats();
        if (fm != null)
          if (fm.Length > 0)
          {
            string newformat = fm[0];
            System.IO.MemoryStream mr = iData.GetData(fm[0]) as System.IO.MemoryStream;



            if (mr != null)
            {
              byte[] dataarray = mr.ToArray();
              var strfrommv = System.Text.Encoding.Default.GetString(dataarray);
            }
          }
        // Determines whether the data is in a format you can use.
        //      RPGCommandMaker.AddCommand(213, 0, new List<object>() { -1, 6, false });
        //      RPGCommandMaker.AddCommand(101, 0, new List<object>() {"",0, 1,0});
        //      RPGCommandMaker.AddCommand(401, 0, new List<object>() { "Bonjour je test" });
      }
      rpgCommandMaker.ClearAll();
      if (richTextBox1.Text.Length > 0)
      {
        List<token> tokenlist = GetToken(richTextBox1.Text);

        string message = "";
        int linecounter = 0;
        string lastwinform = "";
        foreach (token t in tokenlist)
        {
          if (t.Id == TokenId.eWord)
          {
            if (message.Length + t.value.Length < 55)
              message += t.value;
            else
            {
              if (linecounter <= 3)
              {
                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
                message = t.value;
                linecounter++;
              }
              else
              {

                if (lastwinform.IndexOf("<h>") >= 0)
                {
                  rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
                }
                else if (lastwinform.IndexOf("<m>") >= 0)
                {
                  rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 1 });
                }
                else if (lastwinform.IndexOf("<b>") >= 0)
                {
                  rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 2 });
                }
                else
                {
                  rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
                  lastwinform = "";
                }
                linecounter = 0;

                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
                message = t.value;
              }
            }

          }
          else if (t.Id == TokenId.eNewWindowLine)
          {
            if (linecounter <= 3)
            {
              message += t.value;
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
              linecounter++;
              message = "";
            }
            else
            {

              if (lastwinform.IndexOf("<h>") >= 0)
              {
                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
              }
              else if (lastwinform.IndexOf("<m>") >= 0)
              {
                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 1 });
              }
              else if (lastwinform.IndexOf("<b>") >= 0)
              {
                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 2 });
              }
              else
              {
                rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
                lastwinform = "";
              }
              message += t.value;
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
              linecounter = 0;

              
              message = "";
            }
          }
          else if (t.Id == TokenId.eNewPlayerWindow)
          {
            if (message.Length != 0)
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });

            if (t.value.IndexOf("<h>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
              lastwinform = "<h>";
            }
            else if (t.value.IndexOf("<m>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 1 });
              lastwinform = "<m>";
            }
            else if (t.value.IndexOf("<b>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 2 });
              lastwinform = "<b>";
            }
            else
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
              lastwinform = "";
            }

            message = t.value;//.Replace("<m>", "").Replace("<h>", "").Replace("<b>", "");
          }
        }
        if (message.Length != 0)
        {
          if (linecounter <= 3)
          {
            rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
            message = "";
          }
          else
          {

            if (lastwinform.IndexOf("<h>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
            }
            else if (lastwinform.IndexOf("<m>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 1 });
            }
            else if (lastwinform.IndexOf("<b>") >= 0)
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 2 });
            }
            else
            {
              rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eMessage, 0, new List<object>() { "", "", 0, 0 });
              lastwinform = "";
            }
            linecounter = 0;

            rpgCommandMaker.AddCommand(rpgCommandMaker.eRPGCommand.eText, 0, new List<object>() { message });
            message = "";
          }
        }

        string datastr = rpgCommandMaker.GetFormat().ToLower();
        /*string str = richTextBox1.Text;
        // Yes it is, so display it in a text box.
        if (str.IndexOf("<CAMERA ") >= 0)
        {
          string type = str.Substring(str.IndexOf("TYPE")+5, 1).Trim();
          string speed = str.Substring(str.IndexOf("SPEED")+6, 1).Trim();
          string value = str.Substring(str.IndexOf(">")+1, 1).Trim();
          datastr += "[" + type + "," + speed + "," + value + "]}]";
        }
        else
          datastr = "[{\"code\":204,\"indent\":0,\"parameters\":[3,1,4]}]";
        */

        byte[] bytes = Encoding.UTF8.GetBytes(datastr);
        System.IO.MemoryStream stream = new System.IO.MemoryStream(bytes);
        Clipboard.SetData("application/rpgmv-EventCommand", stream);
      }
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    private List<token> GetToken(string file)
    {
      List<token> tokens = new List<token>();
      string word = "";
      foreach (char s in file)
      {

        if (s != '\r')
        {
          if (s == ' ')
          {
            if (word != "")
              tokens.Add(new token(TokenId.eWord, word + s));
            word = "";
          }
          else if (s == '\n')
          {
            //string PlayerName = "\\\\C[6]"+word.Substring(0,1).ToUpper()+word.Substring(1).ToLower()+":\\\\C";
            tokens.Add(new token(TokenId.eNewWindowLine, word));
            word = "";
          }
          else if ((s == '£') || (s == '\n'))
          {
            //string PlayerName = "\\\\C[6]"+word.Substring(0,1).ToUpper()+word.Substring(1).ToLower()+":\\\\C";
            tokens.Add(new token(TokenId.eNewPlayerWindow, word));
            word = "";
          }
          else if ((s != '\n') && (s != ':'))
            word += s;
        }
      }
      if (word.Length > 0)
        tokens.Add(new token(TokenId.eWord, word.Trim()));

      return tokens;
    }
  }

  enum TokenId
  {
    eWord,
    eNewPlayerWindow,
    eNewWindowLine
  };

  class token
  {
    public readonly TokenId Id;
    public readonly string value;

    public token(TokenId id, string val)
    {
      Id = id;
      value = val;
    }
  }
}
